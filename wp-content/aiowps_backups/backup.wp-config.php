<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'gran_app' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'gran_root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'r@hc903@!' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'y7><3N(AH}F[-](CjO>`y9Mf%{yp mk&AA ;oWWi:R}Utr`Sa~X8c_tO{,[)OsjY' );
define( 'SECURE_AUTH_KEY',  ')Gp_$0b{E_FGG{&mAWVTWc^Ks=$lgv%`;;$jK98)MvXUYfA{Z)tdu}@:c{OX{3XT' );
define( 'LOGGED_IN_KEY',    '.BM)D&D(7]hBd_x1mm0jb)aY)2;UfWeWIv^t#dq[%8xPa)e(7:}$&w8X~(B#150z' );
define( 'NONCE_KEY',        'qwsGsUoYS9M4?=:A|p;*SSp*-)vA9E^yjZhX!xNTzlX&%X7EW5GGg *|YD>S*Kai' );
define( 'AUTH_SALT',        '?X*589-Wzdn}.?t.g> m(R{j8AT6cFaWojQxS?#2sDN%qRB(1 WkUV@1*#LEPnLU' );
define( 'SECURE_AUTH_SALT', 'Lq9P|MDtVS1j <p@ug*{MA|RF|DGhc[HVpIOr]#>pf4<b>qo!uYjFMpEN!l(2[+6' );
define( 'LOGGED_IN_SALT',   'Ne(U|w0v{oSymfVv&|Y4hQh<1rWmL@jCs]C})hzDFYSb9CSRTZu)b5WJ[V $vNd[' );
define( 'NONCE_SALT',       '1iL?d|_Xes,jGA3/-{CA]3M(7PI&F(^ TwSq*>;1Lk[|pDx4`_tGgX(5]:I>qdO3' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'ap_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
