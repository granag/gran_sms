$(function(){


	/*************************************************
	 			FUNÇÃO PLACEHOLDER FORMULÁRIO
	*************************************************/
	//ATIVANDO O FOCUS E RECUPERANDO PLACEHOLDER
	$( ".page-template-cadastro-de-clientes  .pg-cadastro-clientes .input_type input[type='text']" ).focusin(function(e) {
	  	let placeholder = $(this).attr('placeholder'); 
	  	
	  	//ATIVA FUNÇÃO VOLTAR PLACEHOLDER
	  	placeholderValor(placeholder);
	 
	 	//RESENTANDO PLACEHOLDER
	 	$(this).attr('placeholder', '');
	 	
	});

	// FUNÇÃO FOCUS AOUT PARA ATIVAR PLACEHOLDER
	function placeholderValor(placeholder){
		$( ".page-template-cadastro-de-clientes  .pg-cadastro-clientes .input_type input[type='text']" ).focusout(function(e){
		   $(this).attr('placeholder', placeholder);
		});
	}

	/*************************************************
	 			FUNÇÃO BUSCA CEP
	*************************************************/
	$(".page-template-cadastro-de-clientes  #cliente_cep").focusout(function(e){
	  const cliente_cep = $(this).val();
	  buscaCep(cliente_cep);
	});

	//REQUEST JASON
	function buscaCep(cliente_cep) {
		const url = 'https://webmaniabr.com/api/1/cep/'+cliente_cep+'/?app_key=KJBVOAc5bQD03qO3bELxe8qqQjQDk1gr&app_secret=YbT7ViLMqOujNzoZcnXrkocpw3J8fqHz1AteWoDpVYxWBHJL'
		
		return fetch(url)
			
			.then( response => {

				const response_cep = response.json();
				response_cep.then(function(result_cep) {
					insertEndereco(result_cep);

				})

	      return response.json();

	    })

	    .catch(e => {
	    	insertEnderecoError()
	      	return false
	    })

	}

	//FUNÇÃO DE PREENCHIMENTO DOS CAMPOS
	function insertEndereco(result_cep){
		$(".page-template-cadastro-de-clientes  #cliente_endereco").val(result_cep.endereco)
		$(".page-template-cadastro-de-clientes  #cliente_bairro").val(result_cep.bairro)
		$(".page-template-cadastro-de-clientes  #cliente_cidade").val(result_cep.cidade)
		$(".page-template-cadastro-de-clientes  #cliente_estado").val(result_cep.uf)
		$(".page-template-cadastro-de-clientes  #cliente_cep").val(result_cep.cep)
	}

	function insertEnderecoError(){
		console.log("Cep invalido");
	}

	/*************************************************
	 	ADCIONAR MASCARAS NOS CAMPOS DE TEXTO 
	*************************************************/
	maskForm();
	function maskForm(){

		let selectedValue = $('#cliente_tipo').find(":selected").val();
		if(selectedValue == "Jurídica"){
			$('#cliente_cnpj_cpf').mask('00.000.000/0000-00', {reverse: true});
		} 

		const $cliente_telefone_comercial = $("#cliente_telefone_comercial");
		$cliente_telefone_comercial.mask('(00) 0000-0000', {reverse: false});

		const $cliente_telefone_celular = $("#cliente_telefone_celular");
		$cliente_telefone_celular.mask('(00)0.00000000', {reverse: false});

		const $cliente_responsavel_cpf = $("#cliente_responsavel_cpf");
		$cliente_responsavel_cpf.mask('000.000.000-00', {reverse: false});

		const $cliente_responsavel_celular = $("#cliente_responsavel_celular");
		$cliente_responsavel_celular.mask('(00)0.00000000', {reverse: false});
	}

	// TROCANDO MASCARA DO CAMPO CPF/CNPJ
	$('#cliente_tipo').change(function(){
		$('#cliente_cnpj_cpf').unmask();
		let selectedValue = $('#cliente_tipo').find(":selected").val();
		if(selectedValue == "Jurídica"){
			$('#cliente_cnpj_cpf').mask('00.000.000/0000-00', {reverse: true});
			if(!$('#cliente_cnpj_cpf').hasClass('campo_input_cnpj_validacao')){
				$('#cliente_cnpj_cpf').addClass('campo_input_cnpj_validacao');
			}
		} else{
			$('#cliente_cnpj_cpf').mask('000.000.000-00', {reverse: false});
			$('#cliente_cnpj_cpf').removeClass('campo_input_cnpj_validacao');
		}
	});

	/*************************************************
	 			FUNÇÃO DE CADASTRO 
	*************************************************/
	var btnCadastrar = document.querySelector(".page-template-cadastro-de-clientes  #btnCadastrar");
	
	//FUNÇÃO CLIQUE
	btnCadastrar.addEventListener("click", function(){ 
		var inputList = document.querySelectorAll(".page-template-cadastro-de-clientes  form input[required='required']")
		validacaoCampos(inputList);
	});

	function validacaoCampos(inputList){
		
		for(let i = 0; i< inputList.length;i++){

			const element      = inputList[i];
			const elementValue = inputList[i].value;

			verificacaoCampos(elementValue,element);
		}
	}

	function verificacaoCampos(elementValue,element){
		if (elementValue == "" || elementValue == undefined){
			element.classList.add("campo-required");
			$('html,body').animate({
				scrollTop: $("#formulario").offset().top
			}, 1000);
		}else{
			element.classList.remove("campo-required");
			$("#cadastrar").trigger('click');
			notificacao_cadastrocliente();
		}
	}

	function notificacao_cadastrocliente(){

		// DADOS EMRPESA
		var cliente_nome_fantasia       = $('#cliente_nome_fantasia').val();
		var cliente_tipo                = $('#cliente_tipo').val();
		var cliente_razao_social        = $('#cliente_razao_social').val();
		var cliente_cnpj_cpf            = $('#cliente_cnpj_cpf').val();
		var cliente_inscricao_estadual  = $('#cliente_inscricao_estadual').val();
		var cliente_inscricao_municipal = $('#cliente_inscricao_municipal').val();
		var cliente_email_principal     = $('#cliente_email_principal').val();
		var cliente_telefone_comercial  = $('#cliente_telefone_comercial').val();
		var cliente_telefone_celular    = $('#cliente_telefone_celular').val();

		// ENDEREÇO
		var cliente_cep                 = $('#cliente_cep').val();
		var cliente_endereco            = $('#cliente_endereco').val();
		var cliente_numero              = $('#cliente_numero').val();
		var cliente_complemento         = $('#cliente_complemento').val();
		var cliente_bairro              = $('#cliente_bairro').val();
		var cliente_cidade              = $('#cliente_cidade').val();
		var cliente_estado              = $('#cliente_estado').val();

		// DADOS RESPONSÁVEL
		var cliente_responsavel_nome_completo = $('#cliente_responsavel_nome_completo').val();
		var cliente_responsavel_celular       = $('#cliente_responsavel_celular').val();
		var cliente_responsavel_email         = $('#cliente_responsavel_email').val();

		if(cliente_nome_fantasia != '' && cliente_responsavel_email != ''){

		  // AJAX
		  $.ajax({

		      url: 'https://app.gran.ag/wp-admin/admin-ajax.php',
		      type: 'POST',
		      data : {
		      	action: "notificacao_cadastrocliente", 

		      	// DADOS EMRPESA
		      	cliente_nome_fantasia :     cliente_nome_fantasia, 
		      	cliente_tipo :              cliente_tipo, 
		      	cliente_razao_social:       cliente_razao_social, 
		      	cliente_cnpj_cpf:           cliente_cnpj_cpf, 
		      	cliente_inscricao_estadual: cliente_inscricao_estadual, 
		      	cliente_inscricao_municipal:cliente_inscricao_municipal, 
		      	cliente_email_principal:    cliente_email_principal, 
		      	cliente_telefone_comercial: cliente_telefone_comercial, 
		      	cliente_telefone_celular:   cliente_telefone_celular, 

		      	// ENDEREÇO
		      	cliente_cep:                cliente_cep, 
		      	cliente_endereco:           cliente_endereco, 
		      	cliente_numero:             cliente_numero, 
		      	cliente_complemento:        cliente_complemento, 
		      	cliente_bairro:             cliente_bairro, 
		      	cliente_cidade:             cliente_cidade, 
		      	cliente_estado:             cliente_estado, 
		      	
		      	// DADOS RESPONSÁVEL
		      	cliente_responsavel_nome_completo:cliente_responsavel_nome_completo,
		      	cliente_responsavel_celular:      cliente_responsavel_celular,
		      	cliente_responsavel_email:        cliente_responsavel_email,
		      },

		      success: function (resp) {

		           console.log("top");

		      }

		  });

		}
	}
		

});