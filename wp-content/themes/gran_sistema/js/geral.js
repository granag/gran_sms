$(function(){

	//BOTÃO ATIVAR MENU
	$("header button").click(function() {
		$("header nav").toggleClass("openNavMenu");
		$("body").toggleClass("travaScroll");
	});
	
	/*************************************************
	 	BUSCA POR CNPJ 
	*************************************************/
	// TRATAR E REMOVER MASCARA DO CNPJ
	function tratarCnpj(cnpj){
		let inputValue = cnpj;

		let treatedCnpj = inputValue.split('.').join('');
		treatedCnpj = treatedCnpj.split('/').join('');
		treatedCnpj = treatedCnpj.split('-').join('');

		return treatedCnpj;
	}

	function buscaCnpj(cnpj){
		let cnpjTratado = tratarCnpj(cnpj);

		const urlBuscaCnpj = 'https://jsonp.afeld.me/?url=https://www.receitaws.com.br/v1/cnpj/'+cnpjTratado;

		return fetch(urlBuscaCnpj)

		.then( response => {
			const resultCnpj = response.json();
			return resultCnpj;
	    })

		.then(function(resultCnpj) {
			if(resultCnpj.status == "ERROR"){
				insertResultCnpjError(resultCnpj);
			} else{ 
				insertResultCnpj(resultCnpj);
			}
		})

	    .catch(e => {
	      	return false;
	    })

	}

	function insertResultCnpj(resultCnpj){
		let inputCnpj = document.querySelector('.cnpj-error-message');
		let element = document.querySelector('.campo_input_cnpj_validacao');

		inputCnpj.classList.remove('cnpj-error-message-active');
		element.classList.remove("campo-required");

		$(".campo_input_nome_fantasia_validacao").val(resultCnpj.nome);
		if(resultCnpj.email != "" && resultCnpj.email != undefined){
			$(".campo_input_email_validacao").val(resultCnpj.email);
		}
		if(resultCnpj.complemento != "" && resultCnpj.complemento != undefined){
			$(".campo_input_complemento_validacao").val(resultCnpj.complemento);
		}
		$(".campo_input_telefone_validacao").val(resultCnpj.telefone);
		$(".campo_input_cep_validacao").val(resultCnpj.cep);
		$(".campo_input_endereco_validacao").val(resultCnpj.logradouro);
		$(".campo_input_numero_validacao").val(resultCnpj.numero);
		$(".campo_input_bairro_validacao").val(resultCnpj.bairro);
		$(".campo_input_cidade_validacao").val(resultCnpj.municipio);
		$(".campo_input_estado_validacao").val(resultCnpj.uf);
	}

	function insertResultCnpjError(resultCnpj){
		let inputCnpj = document.querySelector('.cnpj-error-message');
		let element = document.querySelector('.campo_input_cnpj_validacao');

		inputCnpj.textContent = resultCnpj.message;
		inputCnpj.classList.add('cnpj-error-message-active');
		element.classList.add("campo-required");

		$('html,body').animate({
			scrollTop: $("#formulario").offset().top
		}, 1000);
	}

	$('.campo_input_cnpj_validacao').focusout(function(){
		if($(this).val() != "" && $(this).val() != undefined){
			if($(this).val().length >= 18){
				const colaboradorCnpj = $(this).val();
				buscaCnpj(colaboradorCnpj);
			}
		}
	});

});		