$(function(){

	/*************************************************
	 			FUNÇÃO PLACEHOLDER FORMULÁRIO
	*************************************************/
	//ATIVANDO O FOCUS E RECUPERANDO PLACEHOLDER
	$( ".pg .input_type input[type='text']" ).focusin(function(e) {
	  	let placeholder = $(this).attr('placeholder'); 
	  	
	  	//ATIVA FUNÇÃO VOLTAR PLACEHOLDER
	  	placeholderValor(placeholder);
	 
	 	//RESENTANDO PLACEHOLDER
	 	$(this).attr('placeholder', '');
	 	
	});

	// FUNÇÃO FOCUS AOUT PARA ATIVAR PLACEHOLDER
	function placeholderValor(placeholder){
		$( ".pg .input_type input[type='text']" ).focusout(function(e){
		   $(this).attr('placeholder', placeholder);
		});
	}


	/*************************************************
	 			FUNÇÃO BUSCA CEP
	*************************************************/
	$(".pg #colaborador_cep").focusout(function(e){
	  const cliente_cep = $(this).val();
	  buscaCep(cliente_cep);
	});

	//REQUEST JASON
	function buscaCep(cliente_cep) {
		const url = 'https://webmaniabr.com/api/1/cep/'+cliente_cep+'/?app_key=KJBVOAc5bQD03qO3bELxe8qqQjQDk1gr&app_secret=YbT7ViLMqOujNzoZcnXrkocpw3J8fqHz1AteWoDpVYxWBHJL'
		
		return fetch(url)
			
			.then( response => {

				const response_cep = response.json();
				response_cep.then(function(result_cep) {
					insertEndereco(result_cep);

				})

	      return response.json();

	    })

	    .catch(e => {
	    	insertEnderecoError()
	      	return false
	    })

	}

	//FUNÇÃO DE PREENCHIMENTO DOS CAMPOS
	function insertEndereco(result_cep){
		$(".pg #colaborador_endereco").val(result_cep.endereco)
		$(".pg #colaborador_bairro").val(result_cep.bairro)
		$(".pg #colaborador_cidade").val(result_cep.cidade)
		$(".pg #colaborador_estado").val(result_cep.uf)
		$(".pg #colaborador_cep").val(result_cep.cep)
	}

	function insertEnderecoError(){
		console.log("Cep invalido");
	}


	/*************************************************
	 	ADCIONAR MASCARAS NOS CAMPOS DE TEXTO 
	*************************************************/
	maskForm();
	function maskForm(){

		const $colaborador_clt_cnpj_cpf = $("#colaborador_cpf");
		$colaborador_clt_cnpj_cpf.mask('00.000.000/0000-00', {reverse: true});

		const $colaborador_clt_telefone_comercial = $("#colaborador_felefone_fixo");
		$colaborador_clt_telefone_comercial.mask('(00) 0000-0000', {reverse: false});

		const $colaborador_clt_telefone_celular = $("#colaborador_celular");
		$colaborador_clt_telefone_celular.mask('(00)0.00000000', {reverse: false});

		const $colaborador_clt_rgs = [$("#colaborador_rg"), $("#colaborador_filhos_rg")];
		for(let i = 0; i <= 1; i++){
			$colaborador_clt_rgs[i].mask('00.000.000-0', {reverse: false});
		}

	}


	//BOTÃO CADASTRAR
	var btnCadastrar = document.querySelector(".pg #btnCadastrar");

	btnCadastrar.addEventListener("click", function(e){ 
		var inputList = document.querySelectorAll(".pg input[required='required']")
		validacaoCampos(inputList);
	});

	//FUNCÇÃO DE VALIDAÇÃO DOS CAMPOS
	function validacaoCampos(inputList){

		var inputIsValid = false;
		//PERCORRENDO CAMPOS REQUIRED
		for(let i = 0; i< inputList.length;i++){

			const elementValue = inputList[i].value;
			const element      = inputList[i];

			inputIsValid = verificacaoCampos(elementValue,element);
		}
		if(inputIsValid){
			cadastrarColaboradorClt();
		}
	}

	//FUNÇÃO DE VERIFICAÇÃO DE CAMPOS
	function verificacaoCampos(elementValue,element){
	
		//VERIFICADO CAMPOS VAZIO/SINALIZANDO
		if (elementValue == "" || elementValue == undefined){
		
			element.classList.add("campo-required");
			$('html,body').animate({
				scrollTop: $("#formulario").offset().top
			}, 1000);
			inputIsValid = false;

		}else{

			element.classList.remove("campo-required");
			inputIsValid = true;

		}
		return inputIsValid;
	}

	function recuperarPrimeiroNomeColaborador(){
		let nome_completo = $('#colaborador_nome_completo').val();
		let array_nomes = nome_completo.split(" ");
		let primeiro_nome = array_nomes[0];
		primeiro_nome = primeiro_nome.toLowerCase();

		return primeiro_nome;
	}

	function cadastrarColaboradorClt(){

		// PRIMEIRO NOME DO COLABORADOR
		var colaborador_primeiro_nome  			= recuperarPrimeiroNomeColaborador();

		//DADOS COLABORADOR
		var colaborador_nome_completo  			= $('#colaborador_nome_completo').val();
		var colaborador_nascimento     			= $('#colaborador_nascimento').val();
		var colaborador_rg      				= $('#colaborador_rg').val();
		var colaborador_cpf          			= $('#colaborador_cpf').val();
		var colaborador_sexo		 			= $('#colaborador_sexo').val();
		var colaborador_Pis_Pazep				= $('#colaborador_Pis_Pazep').val();
		var colaborador_celular   				= $('#colaborador_celular').val();
		var colaborador_felefone_fixo 			= $('#colaborador_felefone_fixo').val();
		var colaborador_email 					= $('#colaborador_email').val();
		var colaborador_escolaridade   			= $('#colaborador_escolaridade').val();
		var colaborador_rg_cnh					= $('#colaborador_rg_cnh').val();
		var colaborador_nome_pai   				= $('#colaborador_nome_pai').val();
		var colaborador_nome_mae   				= $('#colaborador_nome_mae').val();
		var colaborador_filhos   				= $('#colaborador_filhos').val();
		var colaborador_nome_completo_filho 	= $('#colaborador_nome_completo_filho').val();
		var colaborador_data_nascimento_filho 	= $('#colaborador_data_nascimento_filho').val();
		var colaborador_filhos_rg 	            = $('#colaborador_filhos_rg').val();
		var colaborador_filhos_certidao   		= $('#colaborador_filhos_certidao').val();

		// ENDEREÇO
		var colaborador_cep               		= $('#colaborador_cep').val();
		var colaborador_endereco          		= $('#colaborador_endereco').val();
		var colaborador_numero            		= $('#colaborador_numero').val();
		var colaborador_complemento       		= $('#colaborador_complemento').val();
		var colaborador_bairro            		= $('#colaborador_bairro').val();
		var colaborador_cidade            		= $('#colaborador_cidade').val();
		var colaborador_estado            		= $('#colaborador_estado').val();

		// if($('#colaborador_nome_completo').val() != '' && $('#colaborador_nascimento').val() != ''){

		  // AJAX
		  $.ajax({

		      url 	: 'http://localhost/projetos/gran_sms/wp-admin/admin-ajax.php',
		      type 	: 'POST',
		      data 	: {
		      action: "cadastrar_colaborador_clt", 

				// PRIMEIRO NOME
				colaborador_primeiro_nome 			:colaborador_primeiro_nome,

		      	// DADOS COLABORADOR
				colaborador_nome_completo 			:colaborador_nome_completo,
				colaborador_nascimento 				:colaborador_nascimento,
				colaborador_rg    					:colaborador_rg,
				colaborador_cpf  					:colaborador_cpf,
				colaborador_sexo					:colaborador_sexo,
				colaborador_Pis_Pazep				:colaborador_Pis_Pazep,
				colaborador_celular   				:colaborador_celular,
				colaborador_felefone_fixo 			:colaborador_felefone_fixo,
				colaborador_email 					:colaborador_email,
				colaborador_escolaridade 			:colaborador_escolaridade,
				colaborador_rg_cnh					:colaborador_rg_cnh,
				colaborador_nome_pai				:colaborador_nome_pai,
				colaborador_nome_mae 				:colaborador_nome_mae,
				colaborador_filhos  				:colaborador_filhos,
				colaborador_nome_completo_filho		:colaborador_nome_completo_filho,
				colaborador_filhos_rg		        :colaborador_filhos_rg,
				colaborador_data_nascimento_filho	:colaborador_data_nascimento_filho,
				colaborador_filhos_certidao 		:colaborador_filhos_certidao,
				colaborador_cep 					:colaborador_cep,
				colaborador_endereco 				:colaborador_endereco,
				colaborador_numero  				:colaborador_numero,
				colaborador_complemento	 			:colaborador_complemento,
				colaborador_bairro  				:colaborador_bairro,
				colaborador_cidade 					:colaborador_cidade,
				colaborador_estado 					:colaborador_estado,
			
		    
		      },

		      success: function (resp) {

		      	console.log("top");
		      	$('span.success-message').addClass('success-message-active');
		      	setTimeout(function(){
		      		location.href = "https://gran.ag/";
		      	}, 3000);

		      }

		  });

		// }
	}
		

});