<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gran_Sistema
 */

?>
<footer>
	<figure>
		<img src="<?php echo get_template_directory_uri(); ?>/img/logo-cinza.svg">
	</figure>
	<p>Copyright 2019 Gran Digital Marketing. <a href="">Política de Privacidade</a></p>
</footer>
<?php wp_footer(); ?>
</body>
</html>
