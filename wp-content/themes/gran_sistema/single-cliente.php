<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Gran_Sistema
 */
$admin_permissao = wp_get_current_user()->caps['administrator'];
global $post;

get_header();
?>

	<div class="pg pg-dados-cadastrais">
			
			<div class="containerLarge">
				
				<section  class="cabecalho">
					<div class="row">
						<div class="col-sm-6">
							<figure>
								<img src="<?php echo rwmb_meta('Gran_cliente_logo')['full_url']; ?>" alt="<?php echo rwmb_meta('Gran_cliente_logo')['full_url']; ?>">
							</figure>
						</div>
						<div class="col-sm-6">
							<div>
								<nav>
									<ul>
										<li>
											<a href="<?php echo get_home_url()."/acessos-cliente/".$post->post_name ; ?>">Acessos</a>
										</li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</section>
				<?php if($admin_permissao):?>
				<section class="dados">
					<h6>Dados Cadastrais</h6>
					<div class="row">
						<div class="col-sm-6">
							<div class="hand-info">
								<h2><?php echo get_the_title(); ?></h2>
								<span>CNPJ: <?php echo rwmb_meta('Gran_cliente_cnpj_cpf'); ?></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="hand-info">
								<h2>RAZÂO SOCIAL</h2>
								<span><?php echo rwmb_meta('Gran_cliente_razao_social'); ?></span>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<div class="hand-info">
								<h2>Comercial</h2>
								<span class="espaco"><?php echo rwmb_meta('Gran_cliente_telefone_comercial'); ?></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-6">
									<div class="hand-info">
										<h2>Celular</h2>
										<span><?php echo rwmb_meta('Gran_cliente_telefone_celular'); ?></span>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="hand-info">
										<h2>E-mail Principal</h2>
										<span><?php echo rwmb_meta('Gran_cliente_email_principal'); ?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<hr>
				<section class="dados">
					<h6>Localização empresarial</h6>
					<div class="row">
						<div class="col-sm-5">
							<div class="hand-info">
								<h2>Endereço</h2>
								<span><?php echo rwmb_meta('Gran_cliente_endereco'); ?></span>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="hand-info">
								<h2>Complemento</h2>
								<span><?php echo rwmb_meta('Gran_cliente_complemento'); ?></span>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="hand-info">
								<h2>Bairro</h2>
								<span><?php echo rwmb_meta('Gran_cliente_bairro'); ?></span>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="hand-info">
								<h2>CEP</h2>
								<span><?php echo rwmb_meta('Gran_cliente_cep'); ?></span>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-2">
							<div class="hand-info">
								<h2>Cidade</h2>
								<span><?php echo rwmb_meta('Gran_cliente_cidade'); ?></span>
							</div>
						</div>

						<div class="col-sm-2">
							<div class="hand-info">
								<h2>Estado</h2>
								<span><?php echo rwmb_meta('Gran_cliente_estado'); ?></span>
							</div>
						</div>

						<div class="col-sm-3">
							<div class="hand-info">
								<h2>Inscrição Estadual</h2>
								<span><?php echo rwmb_meta('Gran_cliente_inscricao_estadual'); ?></span>
							</div>
						</div>

						<div class="col-sm-5">
							<div class="hand-info">
								<h2>Inscrição Municipal</h2>
								<span><?php echo rwmb_meta('Gran_cliente_inscricao_municipal'); ?></span>
							</div>
						</div>

					</div>
				</section>

				<hr>

				<section class="dados">
					<h6>RESPONSÁVEL LEGAL</h6>
					<div class="row">
						<div class="col-sm-4">
							<div class="hand-info">
								<h2>Nome Completo</h2>
								<span><?php echo rwmb_meta('Gran_cliente_responsavel_nome_completo'); ?></span>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="hand-info">
								<h2>CPF</h2>
								<span><?php echo rwmb_meta('Gran_cliente_responsavel_cpf'); ?></span>
							</div>
						</div>
						<div class="col-sm-5">
							<div class="hand-info">
								<h2>Data de nascimento</h2>
								<span><?php echo rwmb_meta('Gran_cliente_responsavel_data_nascimento'); ?></span>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-3">
							<div class="hand-info">
								<h2>Telefone Celular</h2>
								<span><?php echo rwmb_meta('Gran_cliente_responsavel_celular'); ?></span>
							</div>
						</div>
						<div class="col-sm-9">
							<div class="hand-info">
								<h2>Email</h2>
								<span><?php echo rwmb_meta('Gran_cliente_responsavel_email'); ?></span>
							</div>
						</div>
					</div>
				</section>

				<hr>

				<section class="dados">
					<h6>Texto Jurídico</h6>
					<article>
						<p><span><?php echo rwmb_meta('Gran_cliente_razao_social'); ?></span> (<span><?php echo get_the_title(); ?></span>), pessoa <?php echo rwmb_meta('Gran_cliente_tipo'); ?> de direito privado, inscrita no CNPJ/MF sob n.º <?php echo rwmb_meta('Gran_cliente_cnpj_cpf'); ?> com sede na <?php echo rwmb_meta('Gran_cliente_endereco'); ?>, n.º <?php echo rwmb_meta('Gran_cliente_numero'); ?>, <?php echo rwmb_meta('Gran_cliente_complemento'); ?>, <?php echo rwmb_meta('cliente_bairro'); ?>, no município de <?php echo rwmb_meta('Gran_cliente_cidade'); ?>, Estado do <?php echo rwmb_meta('Gran_cliente_estado'); ?>, CEP <?php echo rwmb_meta('Gran_cliente_cep'); ?></p>						
					</article>
					<button>Copiar Testo Jurídico</button>
				</section>
				<?php endif; ?>
			</div>

		</div>

<?php
get_footer();
