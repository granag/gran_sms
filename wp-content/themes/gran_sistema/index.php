<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Gran_Sistema
 */

get_header();
?>

<div class="pg pg-blog">
	
	<section class="secao-posts">
		<h4 class="hidden">SEÇÃO POSTS</h4>
		<div class="full-container">
			<ul class="lista-posts">

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					$categoriasPost = get_the_category();
					foreach ($categoriasPost as $categoria): ?>
				<li>
					<a href="<?php echo get_permalink() ?>" class="link-imagem">
						<figure>
							<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
							<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
						</figure>
					</a>
					<div class="post-content">
						<a href="<?php echo get_category_link($categoria->cat_ID); ?>" class="link-categoria"><?php echo $categoria->name; ?></a>
						<a href="<?php echo get_permalink() ?>" class="link-titulo">
							<h2 class="titulo-post"><?php echo get_the_title() ?></h2>
						</a>
						<span class="data"><?php the_time('j M Y'); ?></span>
					</div>
				</li>
				<?php endforeach; endwhile; endif; ?>
				
			</ul>

			<div class="paginador">
				<?php 
				if(function_exists('pagination')){
					pagination($additional_loop->$max_num_pages);
				}
				?>
			</div>

		</div>
	</section>
</div>


<style type="text/css">
	
	/* 
**
*** BLOG
*** PÁGINA DE POST
**
*/


/* BLOG */
.pg-blog{
	position: relative;
}
	.pg-blog .secao-destaque{

	}
		.pg-blog .secao-destaque .destaque{

		}
			.pg-blog .secao-destaque .destaque .owl-dots{
				position: absolute;
				bottom: 22px;
				left: 50%;
				margin-top: 0 !important;
				-webkit-transform: translateX(-50%);
				-moz-transform: translateX(-50%);
				-ms-transform: translateX(-50%);
				-o-transform: translateX(-50%);
				transform: translateX(-50%);
			}
				.pg-blog .secao-destaque .destaque .owl-dots .owl-dot{
					outline: none;
					margin: 0 5px;
				}
					.pg-blog .secao-destaque .destaque .owl-dots .owl-dot span{
						background: transparent;
						border: 1px solid #fff;
						margin: 0;
					}
				.pg-blog .secao-destaque .destaque .owl-dots .owl-dot.active{

				}
					.pg-blog .secao-destaque .destaque .owl-dots .owl-dot.active span{
						border-color: #000;
						background: #000;
					}
			.pg-blog .secao-destaque .destaque .item{
				background-image: url(../img/postb.png);
				background-repeat: no-repeat;
				background-size: cover;
				background-position: center;
				padding: 68px 0;
				text-align: center;
				outline: none;
				position: relative;
			}
			.pg-blog .secao-destaque .destaque .item::before{
				content: '';
				position: absolute;
				display: block;
				bottom: 0;
				left: 50%;
				width: 100%;
				height: 100%;
				background: linear-gradient(0deg, var(--azul-escuro), transparent);
				-webkit-transform: translateX(-50%);
				-moz-transform: translateX(-50%);
				-ms-transform: translateX(-50%);
				-o-transform: translateX(-50%);
				transform: translateX(-50%);
			}
				.pg-blog .secao-destaque .destaque .item article{
					position: relative;
				}
					.pg-blog .secao-destaque .destaque .item article .link-categoria{
						color: #fff;
						text-transform: uppercase;
						font-size: 12px;
						font-family: var(--kanit);
					}
					.pg-blog .secao-destaque .destaque .item article .titulo{
						color: #fff;
						font-size: 32px;
						margin: 38px 0 35px;
					}
					.pg-blog .secao-destaque .destaque .item article .button-padrao{
						font-size: 12px;
						outline: none;
						padding: 9px 45px;
					}
				.pg-blog .secao-destaque .destaque .item figure{

				}
					.pg-blog .secao-destaque .destaque .item figure img{

					}
	.pg-blog .secao-posts{
		background-color: var(--cinza-claro);
		padding: 60px 0 93px;
		text-align: center;
	}
		.pg-blog .secao-posts ul{
			text-align: left;
			justify-content: space-evenly !important;
		}
			.pg-blog .secao-posts ul li{
				max-width: 23.5% !important;
				margin: 0 0 40px;
			}
				.pg-blog .secao-posts ul li .link-imagem{

				}
					.pg-blog .secao-posts ul li .link-imagem figure{
						margin: 0;
					}
						.pg-blog .secao-posts ul li .link-imagem figure img{

						}
				.pg-blog .secao-posts ul li .link-categoria{
					text-transform: uppercase;
					font-family: var(--kanit) !important;
				}
				.pg-blog .secao-posts ul li .link-titulo{

				}
					.pg-blog .secao-posts ul li .link-titulo .titulo-post{

					}
				.pg-blog .secao-posts ul li .data{

				}
		.pg-blog .secao-posts .paginador{

		}
			.pg-blog .secao-posts .paginador .paginador{
				display: block;
				text-align: center;
			}
				.pg-blog .secao-posts .paginador .paginador a{
					margin: 0;
					display: inline-block;
					width: 100%;
					max-width: 30px !important;
					text-align: center;
					font-family: #000;
					color: #000;
					font-size: 16px;
				}
				.pg-blog .secao-posts .paginador .paginador a.selecionado{
					border-bottom: 1px solid #000;
				}
		.pg-blog .secao-posts .button-padrao{
			margin: 60px 0 0;
		}
/* BLOG */


/* PÁGINA DE POST */
.pg-post{
	background-color: var(--cinza-claro);
}
	.pg-post figure.destaque{
		margin: 0;
		background-size: cover;
		background-position: center;
		background-repeat: no-repeat;
		height: 440px;
	}
		.pg-post figure.destaque img{

		}
	.pg-post a.voltar{
		display: inline-block;
		float: left;
		margin: 51px 0 0 105px;
	}
		.pg-post a.voltar img{
			width: 100%;
			max-width: 16px;
		}
	.pg-post article{
		padding: 54px 0 61px;
	}
		.pg-post article .link-categoria, .pg-post article .titulo-post, .pg-post article .data, .pg-post article p, .pg-post article ul, .pg-post article ol, .pg-post article h3, .pg-post article h4{
			width: 100%;
			max-width: 613px;
			padding: 0 0;
			display: block;
			color: var(--cinza-texto);
			margin: 0 auto 20px;
		}
		.pg-post article .link-categoria{
			color: var(--cinza-texto);
			text-decoration: none;
			text-transform: uppercase;
			font-size: 13px;
			font-family: var(--kanit);
		}
		.pg-post article .titulo-post{
			font-size: 35px;
			line-height: 48px;
			margin: 21px auto 48px;
			color: #000;
		}
		.pg-post article h3, .pg-post article h4{
			color: #000;
			font-family: var(--kanit);
		}
		.pg-post article h3{
			font-size: 28px;
		}
		.pg-post article h4{
			font-size: 24px;
		}
		.pg-post article .data{
			color: #000;
			font-family: #000;
			font-size: 13px;
			text-transform: capitalize;
		}
		.pg-post article p{
			color: var(--cinza-texto);
			line-height: 22px;
		}
		.pg-post article figure{
			text-align: center;
			margin: 60px 0;
		}
			.pg-post article figure img{
				width: 100%;
				max-width: 896px;
			}
		.pg-post article ul{

		}
			.pg-post article ul li{
				list-style-type: disc;
				list-style-position: inside;
			}
		.pg-post article ol{

		}
			.pg-post article ol li{
				list-style-type: decimal;
				list-style-position: inside;
			}
/* PÁGINA DE POST */


/* MEDIA QUERY */
@media(max-width: 1200px){}

@media(max-width: 991px){
	.pg-blog .secao-posts ul li {
		max-width: 31.2% !important;
	}
}

@media(max-width: 830px){
	.pg ul.lista-posts li .post-content {
		padding: 20px 20px 0 !important;
	}
}

@media(max-width: 768px){
	.pg-blog .secao-posts ul li .link-titulo .titulo-post {
		font-size: 18px !important;
	}
	.pg-blog .secao-destaque .destaque .item article .button-padrao {
		padding: 7px 40px;
	}
	.pg-blog .secao-destaque .destaque .item article .titulo {
		font-size: 23px;
		margin: 28px 0 25px;
	}
	.pg-blog .secao-destaque .destaque .item {
		padding: 38px 0;
	}
	.pg-post figure.destaque {
		height: 200px;
	}
}

@media(max-width: 600px){
	.pg-blog .secao-posts ul li {
		max-width: 47.1% !important;
	}
	.pg-blog .secao-posts ul li .link-titulo .titulo-post {
		font-size: 16px !important;
	}
}

@media(max-width: 500px){
	.pg-post article .titulo-post {
		font-size: 28px;
		line-height: 33px;
		margin: 18px auto 38px;
	}
	.pg-post article {
		padding: 24px 0 40px;
	}
}

@media(max-width: 425px){
	.pg-blog .secao-destaque .destaque .item article .button-padrao {
		padding: 5px 40px;
		font-size: 11px;
	}
	.pg-blog .secao-destaque .destaque .item article .titulo {
		font-size: 18px;
		margin: 15px 0 20px;
	}
	.pg-blog .secao-destaque .destaque .item {
		padding: 38px 0;
	}
	.pg-post article figure {
		text-align: center;
		margin: 30px 0;
	}
}

@media(max-width: 370px){
	.pg-blog .secao-posts ul li {
		max-width: 100% !important;
	}
	.pg-post article h4 {
		font-size: 20px;
	}
	.pg-post article h3 {
		font-size: 22px;
	}
}

@media(max-width: 340px){
	.pg-blog .secao-destaque .destaque .item article .titulo {
		font-size: 15px;
		margin: 15px 0 20px;
	}
	.pg-post article .titulo-post {
		font-size: 25px;
		line-height: 28px;
		margin: 18px auto 30px;
	}
}

@media(max-width: 320px){}
</style>
<?php
get_footer();
