<?php 

function cadastrar_colaborador_clt(){

 	/****************************************************
	* DADOS DO COLABORADOR
	*****************************************************/
	    $colaborador_primeiro_nome           = $_POST['colaborador_primeiro_nome'];

	    $colaborador_nome_completo           = $_POST['colaborador_nome_completo'];
	    $colaborador_nascimento              = $_POST['colaborador_nascimento'];
	    $colaborador_rg                      = $_POST['colaborador_rg'];
	    $colaborador_cpf                     = $_POST['colaborador_cpf'];
	    $colaborador_sexo                    = $_POST['colaborador_sexo'];
	    $colaborador_Pis_Pazep               = $_POST['colaborador_Pis_Pazep'];
	    $colaborador_celular                 = $_POST['colaborador_celular'];
	    $colaborador_felefone_fixo           = $_POST['colaborador_felefone_fixo'];
	    $colaborador_email                   = $_POST['colaborador_email'];
	    $colaborador_escolaridade            = $_POST['colaborador_escolaridade'];
	    $colaborador_rg_cnh                  = $_POST['colaborador_rg_cnh'];
	    $colaborador_rg_cnh_name             = $_POST['colaborador_rg_cnh']['name'];
	    $colaborador_nome_pai                = $_POST['colaborador_nome_pai'];
	    $colaborador_nome_mae                = $_POST['colaborador_nome_mae'];
	    $colaborador_filhos                  = $_POST['colaborador_filhos'];
	    $colaborador_nome_completo_filho     = $_POST['colaborador_nome_completo_filho'];
	    $colaborador_filhos_rg               = $_POST['colaborador_filhos_rg'];
	    $colaborador_data_nascimento_filho   = $_POST['colaborador_data_nascimento_filho'];
	    $colaborador_filhos_certidao         = $_POST['colaborador_filhos_certidao'];
	    $colaborador_filhos_certidao_name    = $_POST['colaborador_filhos_certidao']['name'];
	    
	    // ENDEREÇO
	    $colaborador_cep                     = $_POST['colaborador_cep'];
	    $colaborador_endereco                = $_POST['colaborador_endereco'];
	    $colaborador_numero                  = $_POST['colaborador_numero'];
	    $colaborador_complemento             = $_POST['colaborador_complemento'];
	    $colaborador_bairro                  = $_POST['colaborador_bairro'];
	    $colaborador_cidade                  = $_POST['colaborador_cidade'];
	    $colaborador_estado                  = $_POST['colaborador_estado'];

	/****************************************************
	* ARRAY PARA CADASTRAR CLENTES
	*****************************************************/
		$cadastrar_colaborador  = array(
	        'post_title'    => $colaborador_nome_completo,
	        'post_content'  => '',
	        'post_status'   => 'publish',
	        'post_type' 	=> 'colaborador',
	    );	
	    $cliente_ID = wp_insert_post($cadastrar_colaborador);
	    wp_set_object_terms($cliente_ID,32,'categoriacolaborador' );
		
	/****************************************************
	* INSERT METABOXES  DADOS DO CLIENTE
	*****************************************************/
		add_post_meta($cliente_ID, 'Gran_colaborador_nome_completo',        $colaborador_nome_completo,   		true);
		add_post_meta($cliente_ID, 'Gran_colaborador_nascimento',           $colaborador_nascimento,			true);
		add_post_meta($cliente_ID, 'Gran_colaborador_rg',          			$colaborador_rg,               		true);
		add_post_meta($cliente_ID, 'Gran_colaborador_cpf',              	$colaborador_cpf,               	true);
		add_post_meta($cliente_ID, 'Gran_colaborador_sexo',    				$colaborador_sexo,         			true);
		add_post_meta($cliente_ID, 'Gran_colaborador_Pis_Pazep',   			$colaborador_Pis_Pazep,        		true);
		add_post_meta($cliente_ID, 'Gran_colaborador_celular',       		$colaborador_celular,           	true);
		add_post_meta($cliente_ID, 'Gran_colaborador_felefone_fixo',    	$colaborador_felefone_fixo,     	true);
		add_post_meta($cliente_ID, 'Gran_colaborador_email',      			$colaborador_email,           		true);
		add_post_meta($cliente_ID, 'Gran_colaborador_escolaridade',      	$colaborador_escolaridade,      	true);
		add_post_meta($cliente_ID, 'Gran_colaborador_nome_pai',      		$colaborador_nome_pai,          	true);
		add_post_meta($cliente_ID, 'Gran_colaborador_nome_mae',      		$colaborador_nome_mae,          	true);
		add_post_meta($cliente_ID, 'Gran_colaborador_filhos',      			$colaborador_filhos,           		true);
		add_post_meta($cliente_ID, 'Gran_colaborador_nome_completo_filho',  $colaborador_nome_completo_filho,	true);
		add_post_meta($cliente_ID, 'Gran_colaborador_filhos_rg',            $colaborador_filhos_rg,         	true);
		add_post_meta($cliente_ID, 'Gran_colaborador_data_nascimento_filho',$colaborador_data_nascimento_filho,	true);

	/****************************************************
	* INSERT METABOXES DADOS ENDREÇO DO CLIENTE
	*****************************************************/
		add_post_meta($cliente_ID, 'Gran_colaborador_cep',                   		$colaborador_cep,                        true);
		add_post_meta($cliente_ID, 'Gran_colaborador_endereco',              		$colaborador_endereco,                   true);
		add_post_meta($cliente_ID, 'Gran_colaborador_numero',                		$colaborador_numero,                     true);
		add_post_meta($cliente_ID, 'Gran_colaborador_complemento',           		$colaborador_complemento,                true);
		add_post_meta($cliente_ID, 'Gran_colaborador_bairro',                		$colaborador_bairro,                     true);
		add_post_meta($cliente_ID, 'Gran_colaborador_cidade',                		$colaborador_cidade,                     true);
		add_post_meta($cliente_ID, 'Gran_colaborador_estado',                		$colaborador_estado,                     true);

	/****************************************************
	* INSERT METABOXES LOGO DO CLIENTE
	*****************************************************/
		
	move_uploaded_file($colaborador_rg_cnh,WP_CONTENT_DIR .'/uploads/colaboreadores/'.basename($colaborador_rg_cnh_name));

	$logo_nome  = WP_CONTENT_DIR .'/uploads/colaboreadores/' . basename($colaborador_rg_cnh_name);
	$logo_tipo  = wp_check_filetype( basename( $logo_nome ),null);
	$logo_tipos = array('png','jpg','jpeg','gif');

	if(in_array($logo_tipo['ext'], $logo_tipos)){

		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		$diretorioUploads    = wp_upload_dir();
		$logo_detalhes       = array(
			'guid'           => $diretorioUploads['url'] . '/' . basename( $logo_nome ), 
			'post_mime_type' => $logo_tipo['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $logo_nome ) ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		);

		$logo_carregado_Id 	    = wp_insert_attachment($logo_detalhes, $logo_nome, $cliente_ID);
		$logo_carregado_meta	= wp_generate_attachment_metadata($logo_carregado_Id,$logo_nome);

		wp_update_attachment_metadata($logo_carregado_Id, $logo_carregado_meta);
		add_post_meta($cliente_ID, 'Gran_colaborador_rg_cnh',$logo_carregado_Id,true);
	}
		
	if($cliente_ID > 0){ 

		$user_id = username_exists( $colaborador_primeiro_nome );

		if(!$user_id){
			wp_create_user( $colaborador_primeiro_nome, "123", $colaborador_primeiro_nome . "@gran.ag" );
		}

	/****************************************************
	* TEMPLATE DE NOTIFICAÇÃO
	*****************************************************/
	    $html .= '<table align="center" bgcolor="#000000" width="100%"><tr><td align="center"><img width="150" style="margin: 20px 0" src="https://app.gran.ag/wp-content/themes/gran_sistema/img/logo.png" alt="Hand"></td></tr></table>';
	    $html .= '<br />';
	    $html .= '<p><b>Cadastro de colaborador REGIME CLT</b><p> <br />';
	    $html .= '<b>Nome completo</b>: '								. $colaborador_nome_completo         . '<br />';
	    $html .= '<b>Nascimento</b>: '									. $colaborador_nascimento            . '<br />';
	    $html .= '<b>RG</b>: '											. $colaborador_rg                    . '<br />';
	    $html .= '<b>CPF/CNJ</b>: '										. $colaborador_cpf                   . '<br />';
	    $html .= '<b>Gênero</b>: '										. $colaborador_sexo                  . '<br />';
	    $html .= '<b>Pis/Pazep</b>: '									. $colaborador_Pis_Pazep             . '<br />';
	    $html .= '<b>Celular</b>: '										. $colaborador_celular               . '<br />';
	    $html .= '<b>Telefone Fixo</b>: '								. $colaborador_felefone_fixo         . '<br />';
	    $html .= '<b>E-mail</b>: '										. $colaborador_email                 . '<br />';
	    $html .= '<b>Escolaridade</b>: '								. $colaborador_escolaridade          . '<br />';
	    $html .= '<b>Upload foto CNH ou RG</b>: '						. $colaborador_rg_cnh                . '<br />';
	    $html .= '<b>Nome do Pai</b>: '									. $colaborador_nome_pai              . '<br />';
	    $html .= '<b>Nome da mãe</b>: '									. $colaborador_nome_mae              . '<br />';
	    $html .= '<b>Tem Filhos?</b>: '									. $colaborador_filhos                . '<br />';
	    $html .= '<b>Nome completo</b>: '  								. $colaborador_nome_completo_filho   . '<br />';
	    $html .= '<b>Data Nascimento</b>: '								. $colaborador_data_nascimento_filho . '<br />';
	    $html .= '<b>RG</b>: '   										. $colaborador_filhos_rg 			 . '<br />';
	    $html .= '<b>Certidão de Nascimento (Caso não tenha RG)</b>: '	. $colaborador_filhos_certidao       . '<br />';
	    
	    // ENDEREÇO
	    $html .= '<p><b>ENDEREÇO</b></p>';
	    $html .= '<b>CEP:</b>: '               . $colaborador_cep                   . '<br />';
	    $html .= '<b>Endereço:</b>: '          . $colaborador_endereco              . '<br />';
	    $html .= '<b>Número</b>: '             . $colaborador_numero                . '<br />';
	    $html .= '<b>Complemento</b>: '        . $colaborador_complemento           . '<br />';
	    $html .= '<b>Bairro</b>: '             . $colaborador_bairro                . '<br />';
	    $html .= '<b>Cidade</b>: '             . $colaborador_cidade                . '<br />';
	    $html .= '<b>Estado</b>: '             . $colaborador_estado                . '<br />';
	    $html .= '<br />';
	    $html .= '<table align="center" bgcolor="#000000" width="100%"><tr><td align="center"><div style="margin: 20px 0"></div></td></tr></table>';

	    // add_filter( 'wp_mail_content_type', 'set_html_content_type' );
	    // $envio = wp_mail('carolinohudson@gmail.com', 'Novo colaborador cadastrado na GRAN!', $html);
	    // $envio = wp_mail('hudson@gran.ag', 'Novo colaborador cadastrado na GRAN!', $html);
	    // $envio = wp_mail('dev@gran.ag', 'Novo colaborador cadastrado na GRAN!', $html);
	    // $envio = wp_mail('willerson@gran.ag', 'Novo colaborador cadastrado na GRAN!', $html);
	    // remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

    	add_action('wp_ajax_notificacao_cadastroColaborador', 'notificacao_cadastroColaborador');
		add_action('wp_ajax_nopriv_notificacao_cadastroColaborador', 'notificacao_cadastroColaborador');

	}

	die();

}

add_action('wp_ajax_cadastrar_colaborador_clt', 'cadastrar_colaborador_clt');
add_action('wp_ajax_nopriv_cadastrar_colaborador_clt', 'cadastrar_colaborador_clt');

?>