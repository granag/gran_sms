<?php

    function baseGran () {

		// TIPOS DE CONTEÚDO
		modulosGran();

		//TAXONOMIA
		taxonomia_Gran();

		//METABOX
		metaboxesGran();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
	function modulosGran(){

		//POST TYPE CLIENTE
		moduloCliente();

		//POST TYPE ACESSOS
		moduloAcesso();

		//POST TYPE COLABORADOR
		moduloColaborador();

		//POST TYPE BRIEFING DE BRANDING
		moduloBriefingBranding();

	}
	
	// CUSTOM POST TYPE PARCEIROS
	function moduloCliente() {

		$rotulosCliente = array(
								'name'               => 'Clientes',
								'singular_name'      => 'cliente',
								'menu_name'          => 'Clientes',
								'name_admin_bar'     => 'Clientes',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo cliente',
								'new_item'           => 'Novo cliente',
								'edit_item'          => 'Editar cliente',
								'view_item'          => 'Ver cliente',
								'all_items'          => 'Todos os clientes',
								'search_items'       => 'Buscar cliente',
								'parent_item_colon'  => 'Dos clientes',
								'not_found'          => 'Nenhum cliente cadastrado.',
								'not_found_in_trash' => 'Nenhum cliente na lixeira.'
							);

		$argsCliente 	= array(
								'labels'             => $rotulosCliente,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-groups',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'clientes' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('cliente', $argsCliente);

	}

	// CUSTOM POST TYPE ACESSOS
	function moduloAcesso() {

		$rotulosAcesso = array(
								'name'               => 'Acessos',
								'singular_name'      => 'acesso',
								'menu_name'          => 'Acessos',
								'name_admin_bar'     => 'Acessos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo acesso',
								'new_item'           => 'Novo acesso',
								'edit_item'          => 'Editar acesso',
								'view_item'          => 'Ver acesso',
								'all_items'          => 'Todos os acessos',
								'search_items'       => 'Buscar acesso',
								'parent_item_colon'  => 'Dos acessos',
								'not_found'          => 'Nenhum acesso cadastrado.',
								'not_found_in_trash' => 'Nenhum acesso na lixeira.'
							);

		$argsAcesso 	= array(
								'labels'             => $rotulosAcesso,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-unlock',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'acessos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('acesso', $argsAcesso);

	}

	// CUSTOM POST TYPE COLABORADOR
	function moduloColaborador() {

		$rotulosColaborador = array(
								'name'               => 'Colaboradores',
								'singular_name'      => 'colaborador',
								'menu_name'          => 'Colaboradores',
								'name_admin_bar'     => 'Colaboradores',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo colaborador',
								'new_item'           => 'Novo colaborador',
								'edit_item'          => 'Editar colaborador',
								'view_item'          => 'Ver colaborador',
								'all_items'          => 'Todos os colaboradores',
								'search_items'       => 'Buscar colaborador',
								'parent_item_colon'  => 'Dos colaboradores',
								'not_found'          => 'Nenhum colaborador cadastrado.',
								'not_found_in_trash' => 'Nenhum colaborador na lixeira.'
							);

		$argsColaborador 	= array(
								'labels'             => $rotulosColaborador,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-groups',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'colaboradores' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('colaborador', $argsColaborador);

	}

	// CUSTOM POST TYPE BRIEFING DE BRANDING
	function moduloBriefingBranding() {

		$rotulosBriefingBranding = array(
								'name'               => 'Briefings',
								'singular_name'      => 'briefing',
								'menu_name'          => 'Briefings',
								'name_admin_bar'     => 'Briefings',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo briefing',
								'new_item'           => 'Novo briefing',
								'edit_item'          => 'Editar briefing',
								'view_item'          => 'Ver briefing',
								'all_items'          => 'Todos os briefings',
								'search_items'       => 'Buscar briefing',
								'parent_item_colon'  => 'Dos briefings',
								'not_found'          => 'Nenhum briefing cadastrado.',
								'not_found_in_trash' => 'Nenhum briefing na lixeira.'
							);

		$argsBriefingBranding 	= array(
								'labels'             => $rotulosBriefingBranding,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-clipboard',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'briefings' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title' )
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('briefing', $argsBriefingBranding);

	}

	/****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesGran(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}
		function registraMetaboxes( $metaboxes ){

			$prefix = 'Gran_';

			// METABOX DADOS DO CLIENTE
			$metaboxes[] = array(
				'id'			=> 'cliente_detalhes_cliente',
				'title'			=> 'Dados do cliente',
				'pages' 		=> array('cliente'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Logo',
						'id'    => "{$prefix}cliente_logo",
						'desc'  => '',
						'type'  => 'single_image'
					),	
					array(
						'name'  => 'Nome Fantasia',
						'id'    => "{$prefix}cliente_nome_fantasia",
						'desc'  => '',
						'type'  => 'text'
					),	
					array(
						'name'  => 'Tipo de cliente',
						'id'    => "{$prefix}cliente_tipo",
						'desc'  => 'Jurídica ou Física',
						'type'  => 'text'
					),
					array(
						'name'  => 'Razão Social',
						'id'    => "{$prefix}cliente_razao_social",
						'desc'  => '',
						'type'  => 'text'
					),	
					array(
						'name'  => 'CNPJ/CPF',
						'id'    => "{$prefix}cliente_cnpj_cpf",
						'desc'  => '',
						'type'  => 'text'
					),	
					array(
						'name'  => 'Inscrição Estadual',
						'id'    => "{$prefix}cliente_inscricao_estadual",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Inscrição Municipal',
						'id'    => "{$prefix}cliente_inscricao_municipal",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Email Principal',
						'id'    => "{$prefix}cliente_email_principal",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Telefone Comercial',
						'id'    => "{$prefix}cliente_telefone_comercial",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Telefone Celular',
						'id'    => "{$prefix}cliente_telefone_celular",
						'desc'  => '',
						'type'  => 'text'
					),
				),
			);

			// METABOX ENDREÇO DO CLIENTE
			$metaboxes[] = array(
				'id'			=> 'cliente_detalhes_endereco',
				'title'			=> 'Endereço do cliente',
				'pages' 		=> array('cliente'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'CEP',
						'id'    => "{$prefix}cliente_cep",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Endereço',
						'id'    => "{$prefix}cliente_endereco",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Número',
						'id'    => "{$prefix}cliente_numero",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Complemento',
						'id'    => "{$prefix}cliente_complemento",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Bairro',
						'id'    => "{$prefix}cliente_bairro",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Cidade',
						'id'    => "{$prefix}cliente_cidade",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Estado',
						'id'    => "{$prefix}cliente_estado",
						'desc'  => '',
						'type'  => 'text'
					),
					
				),
			);

			// METABOX RESPONSÁVEL CLIENTE
			$metaboxes[] = array(
				'id'			=> 'cliente_detalhes_responsavel',
				'title'			=> 'Responsável legal',
				'pages' 		=> array('cliente'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Nome Completo',
						'id'    => "{$prefix}cliente_responsavel_nome_completo",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'CPF',
						'id'    => "{$prefix}cliente_responsavel_cpf",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Data de Nascimento',
						'id'    => "{$prefix}cliente_responsavel_data_nascimento",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Celular',
						'id'    => "{$prefix}cliente_responsavel_celular",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'E-mail',
						'id'    => "{$prefix}cliente_responsavel_email",
						'desc'  => '',
						'type'  => 'text'
					),
				),
			);

			// METABOX DADOS DO ACESSOS
			$metaboxes[] = array(
				'id'			=> 'cliente_detalhes_acesso',
				'title'			=> 'Dados do acesso',
				'pages' 		=> array('acesso'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Login',
						'id'    => "{$prefix}acesso_login",
						'desc'  => '',
						'type'  => 'text'
					),	
					array(
						'name'  => 'Senha',
						'id'    => "{$prefix}acesso_Senha",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'URL de acesso',
						'id'    => "{$prefix}acesso_url",
						'desc'  => '',
						'type'  => 'text'
					),	
					array(
						'name'  => 'OBS',
						'id'    => "{$prefix}acesso_obs",
						'desc'  => '',
						'type'  => 'textarea'
					),	
				),
			);

			// METABOX DADOS DO COLABORADOR
			$metaboxes[] = array(
				'id'			=> 'colaborador_detalhes',
				'title'			=> 'Dados do colaborador',
				'pages' 		=> array('colaborador'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Nome completo',
						'id'    => "{$prefix}colaborador_nome_completo",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Nascimento',
						'id'    => "{$prefix}colaborador_nascimento",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'RG',
						'id'    => "{$prefix}colaborador_rg",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'CPF',
						'id'    => "{$prefix}colaborador_cpf",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Gênero',
						'id'    => "{$prefix}colaborador_sexo",
						'desc'  => '',
						'type'  => 'text'
					),
					array(	
						'name'  => 'Pis/Pazep',
						'id'    => "{$prefix}colaborador_Pis_Pazep",
						'desc'  => '',
						'type'  => 'text'
					),
					array(	
						'name'  => 'Celular',
						'id'    => "{$prefix}colaborador_celular",
						'desc'  => '',
						'type'  => 'text'
					),
					array(	
						'name'  => 'Telefone Fixo',
						'id'    => "{$prefix}colaborador_felefone_fixo",
						'desc'  => '',
						'type'  => 'text'
					),
					array(	
						'name'  => 'Email',
						'id'    => "{$prefix}colaborador_email",
						'desc'  => '',
						'type'  => 'text'
					),
					array(	
						'name'  => 'Escolaridade',
						'id'    => "{$prefix}colaborador_escolaridade",
						'desc'  => '',
						'type'  => 'text'
					),
					array(	
						'name'  => 'Upload foto CNH ou RG',
						'id'    => "{$prefix}colaborador_rg_cnh",
						'desc'  => '',
						'type'  => 'single_image'
					),
					array(	
						'name'  => 'Nome do Pai',
						'id'    => "{$prefix}colaborador_nome_pai",
						'desc'  => '',
						'type'  => 'text'
					),
					array(	
						'name'  => 'Nome da mãe',
						'id'    => "{$prefix}colaborador_nome_mae",
						'desc'  => '',
						'type'  => 'text'
					),
					array(	
						'name'  => 'Tem Filhos?',
						'id'    => "{$prefix}colaborador_filhos",
						'desc'  => '',
						'type'  => 'text'
					),
					array(	
						'name'  => 'Nome completo do filho',
						'id'    => "{$prefix}colaborador_nome_completo_filho",
						'desc'  => '',
						'type'  => 'text'
					),
					array(	
						'name'  => 'RG Filho',
						'id'    => "{$prefix}colaborador_filhos_rg",
						'desc'  => '',
						'type'  => 'text'
					),
					array(	
						'name'  => 'Data Nascimento Filho',
						'id'    => "{$prefix}colaborador_data_nascimento_filho",
						'desc'  => '',
						'type'  => 'text'
					),
					array(	
						'name'  => 'RG',
						'id'    => "{$prefix}colaborador_filhos_certidao",
						'desc'  => '',
						'type'  => 'single_image'
					),

					
				),
			);

			// METABOX DADOS DO COLABORADOR
			$metaboxes[] = array(
				'id'			=> 'colaborador_detalhes_endereco',
				'title'			=> 'Dados do colaborador - Endereço',
				'pages' 		=> array('colaborador'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'CEP',
						'id'    => "{$prefix}colaborador_cep",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Endereço',
						'id'    => "{$prefix}colaborador_endereco",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Número',
						'id'    => "{$prefix}colaborador_numero",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Complemento',
						'id'    => "{$prefix}colaborador_complemento",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Bairro',
						'id'    => "{$prefix}colaborador_bairro",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Cidade',
						'id'    => "{$prefix}colaborador_cidade",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Estado',
						'id'    => "{$prefix}colaborador_estado",
						'desc'  => '',
						'type'  => 'text'
					),
					
				),
			);

			// METABOX DADOS DO CLIENTE
			$metaboxes[] = array(
				'id'			=> 'colaborador_pj_detalhes_empresa_mei',
				'title'			=> 'Dados da empresa MEI - Regime PJ',
				'pages' 		=> array('colaborador'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Imagem CNH',
						'id'    => "{$prefix}colaborador_pj_cnh",
						'desc'  => '',
						'type'  => 'single_image'
					),	
					array(
						'name'  => 'Imagem CNH texto',
						'id'    => "{$prefix}colaborador_pj_cnh_texto",
						'desc'  => '',
						'type'  => 'text'
					),	
					array(
						'name'  => 'Nome Fantasia',
						'id'    => "{$prefix}colaborador_pj_nome_fantasia",
						'desc'  => '',
						'type'  => 'text'
					),	
					array(
						'name'  => 'Tipo de colaborador',
						'id'    => "{$prefix}colaborador_pj_tipo",
						'desc'  => 'Jurídica ou Física',
						'type'  => 'text'
					),
					array(
						'name'  => 'Razão Social',
						'id'    => "{$prefix}colaborador_pj_razao_social",
						'desc'  => '',
						'type'  => 'text'
					),	
					array(
						'name'  => 'CNPJ',
						'id'    => "{$prefix}colaborador_pj_cnpj",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Email Empresarial',
						'id'    => "{$prefix}colaborador_pj_email_empresarial",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Telefone Comercial',
						'id'    => "{$prefix}colaborador_pj_telefone_comercial",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Telefone Celular',
						'id'    => "{$prefix}colaborador_pj_telefone_celular",
						'desc'  => '',
						'type'  => 'text'
					),
				),
			);

			// METABOX RESPONSÁVEL CLIENTE
			$metaboxes[] = array(
				'id'			=> 'colaborador_pj_detalhes_responsavel',
				'title'			=> 'Responsável legal - Regime PJ',
				'pages' 		=> array('colaborador'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Nome Completo',
						'id'    => "{$prefix}colaborador_pj_responsavel_nome_completo",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'CPF',
						'id'    => "{$prefix}colaborador_pj_responsavel_cpf",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Data de Nascimento',
						'id'    => "{$prefix}colaborador_pj_responsavel_data_nascimento",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Celular',
						'id'    => "{$prefix}colaborador_pj_responsavel_celular",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'E-mail',
						'id'    => "{$prefix}colaborador_pj_responsavel_email",
						'desc'  => '',
						'type'  => 'text'
					),
				),
			);

			// METABOX ENDREÇO DO CLIENTE
			$metaboxes[] = array(
				'id'			=> 'colaborador_pj_detalhes_endereco',
				'title'			=> 'Endereço da empresa - Regime PJ',
				'pages' 		=> array('colaborador'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'CEP',
						'id'    => "{$prefix}colaborador_pj_cep",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Endereço',
						'id'    => "{$prefix}colaborador_pj_endereco",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Número',
						'id'    => "{$prefix}colaborador_pj_numero",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Complemento',
						'id'    => "{$prefix}colaborador_pj_complemento",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Bairro',
						'id'    => "{$prefix}colaborador_pj_bairro",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Cidade',
						'id'    => "{$prefix}colaborador_pj_cidade",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Estado',
						'id'    => "{$prefix}colaborador_pj_estado",
						'desc'  => '',
						'type'  => 'text'
					),

				),
			);

			// BRIEFING DE BRANDING
			// METABOX BRIEFING DE BRANDING DE CONTATO
			$metaboxes[] = array(
				'id'			=> 'briefing_detalhes_contato',
				'title'			=> 'Detalhes do contato',
				'pages' 		=> array('briefing'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Nome do cliente',
						'id'    => "{$prefix}briefing_nome",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Email do cliente',
						'id'    => "{$prefix}briefing_email",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Nome da empresa',
						'id'    => "{$prefix}briefing_nome_empresa",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Website da empresa',
						'id'    => "{$prefix}briefing_website_empresa",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Telefone celular',
						'id'    => "{$prefix}briefing_telefone_celular",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Como nos encontrou',
						'id'    => "{$prefix}briefing_como_encontrou",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Cidade',
						'id'    => "{$prefix}briefing_cidade",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Estado',
						'id'    => "{$prefix}briefing_estado",
						'desc'  => '',
						'type'  => 'text'
					),

				),
			);

			// METABOX BRIEFING DE INFORMAÇÕES BÁSICAS
			$metaboxes[] = array(
				'id'			=> 'briefing_informacoes_basicas',
				'title'			=> 'Informações básicas',
				'pages' 		=> array('briefing'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Projeto novo ou redenho',
						'id'    => "{$prefix}briefing_novo_redesenho",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Quando precisa do projeto',
						'id'    => "{$prefix}briefing_entrega_projeto",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Para o que precisa do projeto/Objetivos',
						'id'    => "{$prefix}briefing_objetivos",
						'desc'  => '',
						'type'  => 'textarea'
					),

				),
			);

			// METABOX BRIEFING DE LISTA DE ENTREGA
			$metaboxes[] = array(
				'id'			=> 'briefing_lista_entrega',
				'title'			=> 'Lista de entrega',
				'pages' 		=> array('briefing'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Precisa de manual de identidade de marca',
						'id'    => "{$prefix}briefing_manual_identidade",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Itens de papelaria',
						'id'    => "{$prefix}briefing_itens_papelaria",
						'desc'  => '',
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Informações extras',
						'id'    => "{$prefix}briefing_informacoes_extras",
						'desc'  => '',
						'type'  => 'textarea'
					),

				),
			);

			// METABOX BRIEFING DE PERFIL DA EMPRESA
			$metaboxes[] = array(
				'id'			=> 'briefing_perfil_empresa',
				'title'			=> 'Perfil da empresa',
				'pages' 		=> array('briefing'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Do que se trata/Produtos/Serviços',
						'id'    => "{$prefix}briefing_produtos_servicos",
						'desc'  => '',
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Descreva em 2 palavras',
						'id'    => "{$prefix}briefing_duas_palavras",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Descreva em 1 palavra',
						'id'    => "{$prefix}briefing_uma_palavra",
						'desc'  => '',
						'type'  => 'text'
					),

				),
			);

			// METABOX BRIEFING SOBRE ASSINATURA
			$metaboxes[] = array(
				'id'			=> 'briefing_sobre_assinatura',
				'title'			=> 'Sobre assinatura',
				'pages' 		=> array('briefing'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Texto assinado na marca',
						'id'    => "{$prefix}briefing_texto_assinado",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Slogan/Tagline',
						'id'    => "{$prefix}briefing_slogan",
						'desc'  => '',
						'type'  => 'text'
					),

				),
			);

			// METABOX BRIEFING DE SOBRE OS CLIENTES
			$metaboxes[] = array(
				'id'			=> 'briefing_sobre_clientes',
				'title'			=> 'Sobre os clientes',
				'pages' 		=> array('briefing'),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(
					array(
						'name'  => 'Descrição dos clientes(gênero, idade, região, poder aquisitivo)',
						'id'    => "{$prefix}briefing_descricao_clientes",
						'desc'  => '',
						'type'  => 'textarea'
					),
					array(
						'name'  => 'Mensagem que deve transmitir',
						'id'    => "{$prefix}briefing_deve_transmitir",
						'desc'  => '',
						'type'  => 'text'
					),
					array(
						'name'  => 'Mensagem que NÃO deve transmitir',
						'id'    => "{$prefix}briefing_nao_deve_transmitir",
						'desc'  => '',
						'type'  => 'text'
					),

				),
			);

    	return $metaboxes;

		}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
		
		function taxonomia_Gran () {
			taxonomiaAcesso();
			taxonomiaColaborador();
		}

		function taxonomiaAcesso() {

			$rotulosCategoriaAcesso = array(

											'name'              => 'Categorias de acesso',
											'singular_name'     => 'Categorias de acessos',
											'search_items'      => 'Buscar categoria do acesso',
											'all_items'         => 'Todas as categorias',
											'parent_item'       => 'Categoria pai',
											'parent_item_colon' => 'Categoria pai:',
											'edit_item'         => 'Editar categoria do acesso',
											'update_item'       => 'Atualizar categoria',
											'add_new_item'      => 'Nova categoria',
											'new_item_name'     => 'Nova categoria',
											'menu_name'         => 'Categorias acesso',
										);

			$argsCategoriaAcesso 	= array(

											'hierarchical'      => true,
											'labels'            => $rotulosCategoriaAcesso,
											'show_ui'           => true,
											'show_admin_column' => true,
											'query_var'         => true,
											'rewrite'           => array( 'slug' => 'acessos-cliente' ),
										);

			register_taxonomy( 'categoriaacesso', array( 'acesso' ), $argsCategoriaAcesso);
		}

		function taxonomiaColaborador() {

			$rotulosCategoriaColaborador = array(

											'name'              => 'Tipos de colaborador',
											'singular_name'     => 'Tipos de colaboradores',
											'search_items'      => 'Buscar tipo de colaborador',
											'all_items'         => 'Todos os tipos',
											'parent_item'       => 'Tipo pai',
											'parent_item_colon' => 'Tipo pai:',
											'edit_item'         => 'Editar tipo de colaborador',
											'update_item'       => 'Atualizar tipo',
											'add_new_item'      => 'Novo tipo',
											'new_item_name'     => 'Novo tipo',
											'menu_name'         => 'Tipos colaborador',
										);

			$argsCategoriaColaborador 	= array(

											'hierarchical'      => true,
											'labels'            => $rotulosCategoriaColaborador,
											'show_ui'           => true,
											'show_admin_column' => true,
											'query_var'         => true,
											'rewrite'           => array( 'slug' => 'tipo' ),
										);

			register_taxonomy( 'categoriacolaborador', array( 'colaborador' ), $argsCategoriaColaborador);
		}

	function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

    /****************************************************
	* AÇÕES
	*****************************************************/

	

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseGran');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseGran();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );

?>