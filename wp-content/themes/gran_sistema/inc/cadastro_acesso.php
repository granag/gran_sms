<?php 
if($_SERVER['REQUEST_METHOD'] == 'POST'){
		
		if(isset($_POST['cadastrar_acesso'])){

			/****************************************************
			* DADOS DO CLIENTE
			*****************************************************/
			// $nome_cliente = $_POST['nome_cliente'];
			$cliente_selecionado_id = $_POST['acesso_tipo'];		
			$acesso_tipo  = $_POST['acesso_tipo'];		
			$acesso_nome  = $_POST['acesso_nome'];		
			$acesso_login = $_POST['acesso_login'];		
			$acesso_Senha = $_POST['acesso_Senha'];		
			$acesso_url   = $_POST['acesso_url'];		
			$acesso_obs   = $_POST['acesso_obs'];		
			
			/****************************************************
			* ARRAY PARA CADASTRAR CLENTES
			*****************************************************/

			// Recuperando taxonomia(todos os dados), através do seu ID.
			$taxonomia_selecionada = get_term_by('term_taxonomy_id', $cliente_selecionado_id, 'categoriaacesso');		

			// Recuperando o nome da taxonomia.
			$nome_cliente = $taxonomia_selecionada->name;

			$cadastrar_acesso  = array(
		        'post_title'    => $acesso_nome." - ".$nome_cliente,
		        'post_content'  => '',
		        'post_status'   => 'publish',
		        'post_type' 	=> 'acesso'
		    );	
		    $acesso_ID = wp_insert_post($cadastrar_acesso);


		    $cat_ids = array($acesso_tipo);
		    $cat_ids = array_map( 'intval', $cat_ids );
			$cat_ids = array_unique( $cat_ids );

			$term_taxonomy_ids = wp_set_object_terms($acesso_ID, $cat_ids, 'categoriaacesso' );
			/****************************************************
			* INSERT METABOXES  DADOS DO CLIENTE
			*****************************************************/
			
			add_post_meta($acesso_ID, 'Gran_acesso_nome',  $acesso_nome,  true);
			add_post_meta($acesso_ID, 'Gran_acesso_login', $acesso_login, true);
			add_post_meta($acesso_ID, 'Gran_acesso_Senha', $acesso_Senha, true);
			add_post_meta($acesso_ID, 'Gran_acesso_url',   $acesso_url,   true);
			add_post_meta($acesso_ID, 'Gran_acesso_obs',   $acesso_obs,   true);
		
			if($acesso_ID > 0){ $cadastroRealizado = true;}
		}
	}

?>