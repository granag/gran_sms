<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Gran_Sistema
 */
$admin_permissao = wp_get_current_user()->caps['administrator'];
// $user_id = username_exists( $user_name );
// wp_create_user( "Teste", "123", "hudson@gmail.com" );


?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header>

	<div class="containerFull">
		<button>
			<span></span>
			<span></span>
			<span></span>
		</button>
		<div class="row">
			<div class="col-sm-3">
				<figure>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg">
					</a>
				</figure>
			</div>		
			<div class="col-sm-9">
				
				<nav>
					<button>
						<img src="img/cancel.svg">
					</button>
					<ul>

						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Clientes</a></li>
						<?php if($admin_permissao):?>
						<li><a href="<?php echo esc_url( home_url( '/cadastro-de-clientes/' ) ); ?>">Cadastrar novo cliente</a></li>
						<?php endif; ?>
						<?php if($admin_permissao):?>
						<li><a href="<?php echo esc_url( home_url( '/cadastro-de-colaborador-regime-clt/' ) ); ?>">Cadastrar CLT</a></li>
						<?php endif; ?>
						<?php if($admin_permissao):?>
						<li><a href="<?php echo esc_url( home_url( '/cadastro-de-colaborador-regime-pj/' ) ); ?>">Cadastrar PJ</a></li>
						<?php endif; ?>
						<li><a href="<?php echo esc_url( home_url( '/cadastro-de-acessos/' ) ); ?>">Cadastro de acessos</a></li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</header>
