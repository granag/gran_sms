
<?php

/**
 * Template Name: Cadastro de funcionario
 * Description: Cadastro de funcionario
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */

// require get_template_directory() . '/inc/cadastro_colaborador_clt.php';

get_header(); ?>
<div class="pg pg-cadastro-colaboradores">
	<div class="containerLarge" id="formulario">
		<!-- <form  id="formulario" method="post"  enctype="multipart/form-data" id="conlaborador"> -->
			<section class="sectionForm">
				<h2 class="hand-title-page">Cadastro de colaborador <br> REGIME CLT</h2>

				<div class="row">
					<div class="col-sm-12">
						<div class="form_box">
							<strong class="hand-title-form">Dados do Colaborador</strong>
							<hr>
							
							<div class="row">
								
								<div class="col-sm-5">
									<div class="row">
										<div class="col-sm-8">
											<div class="input_type">
												<label>Nome completo</label>
												<input type="text" required="required" placeholder="Fabiana Hage de Souza" name="colaborador_nome_completo" id="colaborador_nome_completo">
											</div>
										</div>
										<div class="col-sm-4">
											<div class="input_type">
												<label>Nascimento</label>
												<input type="date" placeholder="25/07/1990" name="colaborador_nascimento" id="colaborador_nascimento">
											</div>
										</div>
									</div>
								</div>

								<div class="col-sm-7">
									<div class="row">
										<div class="col-sm-3">
											<div class="input_type">
												<label>RG</label>
												<input type="text" required="required" placeholder="8.547.270-4" name="colaborador_rg" id="colaborador_rg">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="input_type">
												<label>CPF</label>
												<input type="text" required="required" placeholder="047.611.819-07" name="colaborador_cpf" id="colaborador_cpf">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="input_type">
												<label>Gênero</label>
												<select name="colaborador_sexo" id="colaborador_sexo">
													<option value="Masculino">Masculino</option>
													<option value="Feminino">Feminino</option>
												</select>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="input_type">
												<label>Pis/Pazep</label>
												<input type="text" placeholder="8.89765" name="colaborador_Pis_Pazep" id="colaborador_Pis_Pazep">
											</div>
										</div>
									</div>
								</div>

							</div>

							<div class="row">
								
								<div class="col-sm-5">
									<div class="col-sm-6">
										<div class="input_type">
											<label>Celular</label>
											<input type="text" placeholder="(41) 9.9941-3280" name="colaborador_celular" id="colaborador_celular">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="input_type">
											<label>Telefone Fixo</label>
											<input type="text" placeholder="(41) 3121-2435" name="colaborador_felefone_fixo" id="colaborador_felefone_fixo">
										</div>
									</div>
								</div>
								<div class="col-sm-7">
									<div class="col-sm-6">
										<div class="input_type">
											<label>Email</label>
											<input type="text" placeholder="felipe@stardente.com.br" name="colaborador_email" id="colaborador_email">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="input_type">
											<label>Escolaridade</label>
											<select name="colaborador_escolaridade" id="colaborador_escolaridade">
												<option value="Médio completo">Médio completo</option>
												<option value="Superior cursando">Superior cursando</option>
												<option value="Superior completo">Superior completo</option>
												<option value="Pós-graduação cursando">Pós-graduação cursando</option>
												<option value="Pós-graduação completo">Pós-graduação completo</option>
												<option value="Mestrado cursando">Mestrado cursando</option>
												<option value="Mestrado completo">Mestrado completo</option>
												<option value="Doutorado cursando">Doutorado cursando</option>
												<option value="Doutorado completo">Doutorado completo</option>
												<option value="Técnico cursando">Técnico cursando</option>
												<option value="Técnico completo">Técnico completo</option>
											</select>
										</div>
									</div>
								</div>

							</div>

							<div class="row">
								<div class="col-sm-6">
									<p>Upload foto CNH ou RG</p>
									<div class="row">
										<div class="col-sm-6">
											
											<div class="input_type">
												<button class="btnSubmit"><input type="file" id="colaborador_rg_cnh" value=""  name="colaborador_rg_cnh">Escolher arquivo</button>
												
											</div>
										</div>
										<div class="col-sm-6">
											<!-- <div class="input_type">
												<p class="nameArquive">NOME_Certidão.PDF</p>
											</div> -->
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-6">
											<div class="input_type">
												<label>Nome do Pai</label>
												<input type="text" placeholder="Eloi milton Pundrich" name="colaborador_nome_pai" id="colaborador_nome_pai">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="input_type">
												<label>Nome da mãe</label>
												<input type="text" placeholder="Maria Gessi dos Santos Pundrich" name="colaborador_nome_mae" id="colaborador_nome_mae">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								
								<div class="col-sm-6">
									
									<div class="row">
										<div class="col-sm-3">
											<div class="input_type">
												<label>Tem Filhos?</label>
												<select name="colaborador_filhos" id="colaborador_filhos">
													<option value="Sim">Sim</option>
													<option value="Não">Não</option>
												</select>
											</div>
										</div>
										<div class="col-sm-9">
											<div class="input_type">
												<label>Nome completo</label>
												<input type="text" placeholder="Fabiana Hage de Souza" name="colaborador_nome_completo_filho" id="colaborador_nome_completo_filho">
											</div>
										</div>
									</div>

								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-6">
											<div class="row">
												<div class="col-sm-6">
													<div class="input_type">
														<label>Data Nascimento</label>
														<input type="date" name="colaborador_data_nascimento_filho" id="colaborador_data_nascimento_filho" >
													</div>
												</div>
												<div class="col-sm-6">
													<div class="input_type">
														<label>RG</label>
														<input type="text" placeholder="8.547.270-4" name="colaborador_filhos_rg" id="colaborador_filhos_rg">
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<p>Certidão de Nascimento (Caso não tenha RG)</p>
											<div class="row">
												<div class="col-sm-7">
													
													<div class="input_type">
														<button class="btnSubmit"><input type="file" value=""  name="colaborador_filhos_certidao" id="colaborador_filhos_certidao"> Escolher arquivo</button>
													</div>
												</div>
												<div class="col-sm-5">
													<div class="input_type">
														<!-- <p class="nameArquive">NOME_Certidão.PDF</p> -->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
			</section>
			<hr>
			
			<section class="sectionForm">

					<div class="row">
						<div class="col-sm-12">
							<div class="form_box">
								<strong class="hand-title-form">Endereço</strong>
								
								<div class="row">
									<div class="col-sm-3">
										<div class="input_type">
											<label>CEP</label>
											<input type="text" placeholder="81.550.340" name="colaborador_cep" id="colaborador_cep">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="input_type">
											<label>Endereço</label>
											<input type="text" placeholder="Rua Marechal Deodoro" name="colaborador_endereco" id="colaborador_endereco">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="input_type">
											<label>Número</label>
											<input type="text" placeholder="807" name="colaborador_numero" id="colaborador_numero">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-3">
										<div class="input_type">
											<label>Complemento</label>
											<input type="text" placeholder="Sala 890" name="colaborador_complemento" id="colaborador_complemento">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="input_type">
											<label>Bairro</label>
											<input type="text" placeholder="Centro" name="colaborador_bairro" id="colaborador_bairro">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="input_type">
											<label>Cidade</label>
											<input type="text" placeholder="Curitiba" name="colaborador_cidade" id="colaborador_cidade">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="input_type">
											<label>Estado</label>
											<input type="text" placeholder="Paraná" name="colaborador_estado" id="colaborador_estado">
										</div>
									</div>
								</div>

							</div>
						</div>
					
					</div>

					<span class="success-message">Cadastro realizado com sucesso! Você será redirecionado para a página inicial.</span>
			</section>

			<div class="input_type_general">
				<!-- <input type="hidden" name="cadastrar_colaborador" value="1" class="btnSubmit"> -->
				<!-- <input type="submit" id="cadastrar" value="Cadastrar" class="hidden"> -->
				<input type="" id="btnCadastrar" value="Cadastrar" name="cadastro-colaborador-clt">
			</div>
		<!-- </form> -->
	</div>
</div>
<?php get_footer();