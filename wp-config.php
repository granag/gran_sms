<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_gran_sms' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '123' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'R`}oFk&6cF{&r!o& .1UAY;&2iyTA02f!}>5Ky&+j!a69%LdjFBau=<wNk2f |3P' );
define( 'SECURE_AUTH_KEY',  'uWg`pMXI|oeW!UqX8GJpm> 3&ze!SC&/@tO#;7YYmpNKF=b%-zjOJ, O9q2U&=Kw' );
define( 'LOGGED_IN_KEY',    'hM`&hb@zI`NGt#GeROe:6gx~tj5sHZ/;m(jCXs`):C/fZ}?Yz=8;Wshr_2N]<jW[' );
define( 'NONCE_KEY',        'R8a7Btf<c?$Dr,Bjv_p{+oQ3@CC@Gky}1#^7iPkEC/VYDtR9G(eMVSs)~>2G9i@n' );
define( 'AUTH_SALT',        '?MMV+3=;U71glXJm$UV8!WzBaiDkM@[?1FfL<d~6DdqJfGGIE-%]v.+7`{k;j;*Z' );
define( 'SECURE_AUTH_SALT', 'oD2fp?_Bj<nOWU`*W6v,]bkbh*J75sy]cmXqW4A-(*`$%+`k#fRp2C`q,(|Nje39' );
define( 'LOGGED_IN_SALT',   'bmJqo=;jlee~1bw~SZ)=[+o4_X@,H{<+PQKy+l*Ix6SE:{tMVl<%otMM;$=Tw@9&' );
define( 'NONCE_SALT',       ',4>Y+!qc-fdv]|cNeT#V7aPZ%Q>O(K[b|wROtm5qr3$y^& N,0UJ9G%3J3>K,%2=' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'gr_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
